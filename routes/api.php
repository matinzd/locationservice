<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('location/setLocation/', function (Request $request){
    $json = $request->json();
    DB::table('location')->insert([
        'latitude' => $json->get('latitude'),
        'longitude' => $json->get('longitude')
    ]);
});
